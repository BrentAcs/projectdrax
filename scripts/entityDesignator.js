class EntityDesignator{
   static Letters(){
      return "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
   }
   
   static Numbers(){
      return "0123456789";
   }
   
   static AlphaNumeric(){
      return EntityDesignator.Letters() + EntityDesignator.Numbers();
   }

   static generate( templ ){
      var result = "";

      if(0===arguments.length){
         templ = "AAA-AAA";
      }

      for( var i = 0; i < templ.length; i++ ) {
         switch( templ[ i ] ) {
            // add random letter
            case 'L':
               result += EntityDesignator.Letters()[ Math.floor( Math.random() * EntityDesignator.Letters().length ) ];
               break;
            // add random number
            case 'N':
               result += EntityDesignator.Numbers()[ Math.floor( Math.random() * EntityDesignator.Numbers().length ) ];
               break;
            // add random letter or number
            case 'A':
               result += EntityDesignator.AlphaNumeric()[ Math.floor( Math.random() * EntityDesignator.AlphaNumeric().length ) ];
               break;
            // add exact
            default:
               result += templ[ i ];
         }
      }

      return result;
   }
}