class GalaxyDefaults{
   static get xRadius() {
      return 20;
   }

   static get yRadius(){
      return 20;
   }

   static get zRadius(){
      return 10;
   }
}
