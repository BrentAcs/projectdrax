const _rnd = new Random();
const _precision = 1000;
const _fractionDigits = _precision.toString().length;
let _systems = null;

function initApp() {
   console.log( "app init ...");

   _systems  = LocalStore.Get("systems");
   if( null == _systems ){
      console.log( "Creating new systems ...");
      _systems = [];

      for ( i = 0; i < 5; i++ ) {
         let designator = EntityDesignator.generate();
         let location = Point3d.CreateRandom( -20, 20, -20, 20, -10, 10 );
         let system = new SolarSystem( designator, location );

         _systems.push( system );
      }

      LocalStore.Set( "systems", _systems );
   }
   else{
		console.log( "Systems loaded ...");
   }

   runTests();
}

function unloadApp(){

   return "";
}
