class Point3d {
   constructor( x, y, z ) {
      switch ( arguments.length ) {
         case 0:
            this.x = this.y = this.z = 0;
            break;

         case 1:
            this.x = this.y = this.z = x;
            break;

         case 3:
            this.x = x;
            this.y = y;
            this.z = z;
            break;

         default:
            throw new Error( "Invalid number of arguments." );
      }
   }

   static CreateRandom( minX, maxX, minY, maxY, minZ, maxZ ) {
      let x = _rnd.Next( minX * _precision, maxX * _precision ) / _precision;
      let y = _rnd.Next( minY * _precision, maxY * _precision ) / _precision;
      let z = _rnd.Next( minZ * _precision, maxZ * _precision ) / _precision;

      return new Point3d( x, y, z );
   }
}

Point3d.prototype.toString = function () {
   return "[ x:{0}, y:{1}, z:{2} ]".format(
      this.x.toFixed( _fractionDigits ),
      this.y.toFixed( _fractionDigits ),
      this.z.toFixed( _fractionDigits ) );
};

Point3d.prototype.distanceTo = function ( rhs ) {
   var a = this.x - rhs.x;
   var b = this.y - rhs.y;
   var c = this.z - rhs.z;

   return Math.sqrt( (a * a) + (b * b) + (c * c) );
};
