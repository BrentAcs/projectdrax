
class SolarSystem{
	constructor( d, l ){
		if( undefined === d ){
			d = EntityDesignator.generate();
		}
		if( undefined === l ){
			l = new Point3d();
		}

		this._designator = d;
		this._location = l;
		this._id = ++SolarSystem._nextId;
	}

	static get NextId() {
		return SolarSystem._nextId;
	}

	get id(){
		return this._id;
	}

   get designator() {
      return this._designator;
   }

   get location(){
		return this._location;
	}
	set location(l){
		this._location = l;
	}
}

SolarSystem._nextId = 0;

SolarSystem.prototype.toString = function () {
   return "[ Id:{0}, Designator:{1}, Location:{2} ]".format(
   	this._id,
		this._designator,
		this._location.toString()
	);
};
