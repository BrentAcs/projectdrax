class LocalStorageTests {
   static canGetSetItem(){
      LocalStore.Set( "testItem", "testValue" );
      let item = LocalStore.Get( "testItem" );

      MyAssert.isValid( item === "testValue", "test failed." );
      LocalStore.Clear();
   }
}
